.PONY: i t it

COMPOSER = $(shell which composer)
PHPUNIT = vendor/bin/phpunit
PHP = $(shell which php)

t:
	XDEBUG_MODE=coverage vendor/bin/phpunit --coverage-text --colors=never

i:
	$(COMPOSER) install -o -a
