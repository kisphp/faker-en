<?php

namespace Kisphp\Faker\En\Provider;

use Kisphp\Faker\AbstractProvider;

class Address extends AbstractProvider
{
    protected $address = ['Schlosstrasse', 'Birkbuschstasse', 'Friedrichstrasse'];
}