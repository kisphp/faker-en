<?php

namespace Kisphp\Faker\En\Provider;

use Kisphp\Faker\AbstractProvider;

/**
 * @property $age
 * @property $firstNameMale
 * @property $firstNameFemale
 * @property $firstName
 * @property $lastName
 * @property $prefix
 * @property $suffix
 */
class Person extends AbstractProvider
{
    protected $firstName = [];
    protected $firstNameMale = ['John', 'Nicholas'];
    protected $firstNameFemale = ['Alice', 'Donna'];
    protected $lastName = ['Smith', 'Jackson'];
    protected $prefix = ['Mr.', 'Ms.'];
    protected $suffix = ['Phd.', 'Dr.'];
    protected $age = [];

    public function __construct()
    {
        $this->age = range(10, 90);
        $this->firstName = array_merge(
            $this->firstNameFemale,
            $this->firstNameMale,
        );
    }
}
