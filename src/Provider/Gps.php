<?php

namespace Kisphp\Faker\En\Provider;

use Kisphp\Faker\AbstractProvider;

class Gps extends AbstractProvider
{
    protected $latitude = ['43'];

    protected $longitude = ['21'];
}