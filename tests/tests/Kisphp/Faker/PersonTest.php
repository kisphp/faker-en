<?php

namespace tests\Kisphp\Faker;

use Kisphp\Faker\En\Provider\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    public function testFirstNameMale()
    {
        $p = new Person();

        $this->assertContains('John', $p->getProperty('firstNameMale'));
    }

    public function testFirstNameMaleNoProperty()
    {
        $p = new Person();

        $this->expectException(\Exception::class);

        $p->getProperty('firstNameChild');
    }

    public function testAge()
    {
        $p = new Person();

        $age = $p->age;

        $this->assertTrue($age >= 10 && $age <= 90);
    }

    public function testFirstName()
    {
        $p = new Person();

        $this->assertNotNull($p->firstName);
    }

    public function testLastName()
    {
        $p = new Person();

        $this->assertNotNull($p->lastName);
    }

    public function testPrefix()
    {
        $p = new Person();

        $this->assertNotNull($p->prefix);
    }

    public function testSuffix()
    {
        $p = new Person();

        $this->assertNotNull($p->suffix);
    }
}
